const db = require('../models');
const config = require('../config/config.json');
const User = db.User;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.signup = (req, res) => {
    console.log('Processing sign up');

    User.create({
        login: req.body.login,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        createdAt: new Date(),
        updatedAt: new Date()
    })
        .then(() => {
            res.status(200).send();
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.signin = (req, res) => {
    console.log('Processing sign in');

    User.findOne({
        where: {
            email: req.body.email
        },
    })
        .then(user => {
            if (!user) {
                return res.status(404).send('User not found');
            }

            const passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );
            if (!passwordIsValid) {
                return res.status(401).send({
                    auth: false,
                    accessToken: null,
                    reason: 'InvalidPassword'
                });
            }

            const token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 86400 * 7
            });

            console.log(token);
            res.status(200).send({ auth: true, accessToken: token, user: {login: user.login, email: user.email} });
        })
        .catch(err => {
            res.status(500).send(`Error: ${err}`);
        });
};

exports.userContent = (req, res) => {
    let token = req.headers['x-access-token'];
    User.findOne({
        where: { id: req.userId },
        attributes: ['login', 'email']
    })
        .then(user => {
            if (!user) {
                return res.status(404).send('User not found');
            }

            res.status(200).json({
                auth: true,
                accessToken: token,
                user: user
            });
        })
        .catch(err => {
            res.status(500).json({
                description: 'Can not access User Page',
                err: err
            });
        });
};
