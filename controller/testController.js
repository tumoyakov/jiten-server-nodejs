const db = require('../models');
const config = require('../config/config.json');
const Sequelize = db.Sequelize;
const Op = Sequelize.Op;
const KanjiListStat = db.KanjiListStat;
const KanjiStat = db.KanjiStat;
const WordListStat = db.WordListStat;
const WordStat = db.WordStat;

/**
 * req.body.statisic = {
 *  listId,
 *  type: 'kanji' or 'word',
 *  test_type: 0, 1, 2, 3 or 4
 *  items = [{ itemId, solved: true or false}]
 * }
 */
exports.setStatistic = async (req, res) => {
  const { statistic } = req.body;
  if (statistic && statistic.items && statistic.items.length > 0) {
    if (type === 'kanji') {
      await KanjiListStat.findOne({
        where: { kanjiListId: statistic.listId },
      })
        .then((list) => {
          const date = new Date();
          const success = statistic.items.filter((item) => item.solved === true)
            .length;
          const result = success / statistic.items.length;
          const test = {
            date,
            test_type: statistic.test_type,
            result,
            success,
            all: statistic.items.length,
          };
          if (list) {
            let tests = JSON.parse(list.dataValues.tests);
            tests = tests.push(test);
            KanjiListStat.update(
              { tests },
              {
                where: { kanjiListId: statistic.listId },
              }
            );
          } else {
            KanjiListStat.create({ test });
          }

          for (let item of statistic.items) {
            KanjiStat.findOne({
              where: { kanjiId: item.itemId, userId: user },
            }).then((kanjiStat) => {
              const attempt = {
                date,
                solved: item.solved,
                test_type: statistic.test_type,
              };
              if (kanjiStat) {
                KanjiStat.update(
                  { attempts: [attempt] },
                  { where: { kanjiId: item.itemId, userId: req.userId } }
                );
              } else {
                KanjiStat.create({
                  attempts: [attempt],
                  kanjiId: item.itemId,
                  userId: req.userId,
                });
              }
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
      return res.status(200).send();
    } else {
      await WordListStat.findOne({
        where: { wordListId: statistic.listId },
      })
        .then((list) => {
          const date = new Date();
          const success = statistic.items.filter((item) => item.solved === true)
            .length;
          const result = success / statistic.items.length;
          const test = {
            date,
            test_type: statistic.test_type,
            result,
            success,
            all: statistic.items.length,
          };
          if (list) {
            let tests = JSON.parse(list.dataValues.tests);
            tests = tests.push(test);
            WordListStat.update(
              { tests },
              {
                where: { wordListId: statistic.listId },
              }
            );
          } else {
            WordListStat.create({ test });
          }

          for (let item of statistic.items) {
            WordStat.findOne({
              where: { wordId: item.itemId, userId: req.userId },
            }).then((wordStat) => {
              const attempt = {
                date,
                solved: item.solved,
                test_type: statistic.test_type,
              };
              if (wordStat) {
                WordStat.update(
                  { attempts: [attempt] },
                  { where: { wordId: item.itemId, userId: req.userId } }
                );
              } else {
                WordStat.create({
                  attempts: [attempt],
                  wordId: item.itemId,
                  userId: req.userId,
                });
              }
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });

      return res.status(200).send();
    }
  } else {
    return res.status(500).send('emptyData');
  }
};
