const _ = require('lodash');
const db = require('../../models');
const Kanji = db.Kanji;
const Radical = db.Radical;
const KanjiDicRef = db.KanjiDicRef;
const KanjiQueryCode = db.KanjiQueryCode;
const KanjiVariant = db.KanjiVariant;
const KanjiReading = db.KanjiReading;
const KanjiMeaning = db.KanjiMeaning;
const Nanori = db.Nanori;
const { Op } = db.Sequelize;

const Radk = db.Radk;
const RadToKanji = db.RadToKanji;

exports.getKanji = (req, res) => {
  const { id } = req.params;
  Kanji.findOne({
    where: { id: id },
    include: [
      { model: Radical },
      { model: KanjiDicRef, as: 'kanjiDicRefs' },
      { model: KanjiQueryCode, as: 'kanjiQueryCodes' },
      { model: KanjiVariant, as: 'kanjiVariants' },
      { model: KanjiReading, as: 'kanjiReadings' },
      { model: KanjiMeaning, as: 'kanjiMeanings' },
      { model: Nanori, as: 'nanories' },
    ],
  })
    .then((kanji) => {
      if (!kanji)
        return res.status(404).send({
          error: { name: 'Kanji not found' }, //TODO add translation
        });

      return res.status(200).send({
        kanji: kanji,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        err: err.toString(), //TODO add translation
      });
    });
};

exports.getKanjis = (req, res) => {
  const { ids } = req.body;
  Kanji.findAll({
    where: { id: { [Op.in]: ids } },
    include: [
      { model: Radical },
      { model: KanjiDicRef, as: 'kanjiDicRefs' },
      { model: KanjiQueryCode, as: 'kanjiQueryCodes' },
      { model: KanjiVariant, as: 'kanjiVariants' },
      { model: KanjiReading, as: 'kanjiReadings' },
      { model: KanjiMeaning, as: 'kanjiMeanings' },
      { model: Nanori, as: 'nanories' },
    ],
  })
    .then((kanjis) => {
      if (!kanjis)
        return res.status(404).send({
          error: { name: 'Kanji not found' }, //TODO add translation
        });

      return res.status(200).send({
        kanjis,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        err: err.toString(), //TODO add translation
      });
    });
};

exports.getRadk = (req, res) => {
  Radk.findAll()
    .then((rads) => {
      if (!rads)
        return res.status(404).send({
          error: { name: 'Radicals not found' }, //TODO add translation
        });

      return res.status(200).send({
        rads,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        err: err.toString(), //TODO add translation
      });
    });
};

exports.findKanjisByRads = (req, res) => {
  const rads = req.body.rads;
  Radk.findAll({
    where: {
      id: {
        [Op.in]: rads,
      },
    },
    include: {
      model: Kanji,
    },
  })
    .then(async (result) => {
      if (!result)
        return res.status(404).send({
          error: { name: 'Kanjis not found' }, //TODO add translation
        });

      let kanjiIds = [];
      if (result.length > 1) {
        kanjiIds = result[0].dataValues.Kanjis.map((kanji) => kanji.id);
        for (let i = 1; i < result.length; i++) {
          kanjiIds = kanjiIds.filter((item) => {
            return (
              result[i].dataValues.Kanjis.findIndex(
                (kanji) => kanji.id === item
              ) !== -1
            );
          });
        }
      } else {
        kanjiIds = result[0].dataValues.Kanjis.map((kanji) => kanji.id);
      }

      let kanjis = [];
      for (kanjiId of kanjiIds) {
        const kanji = await Kanji.findOne({ where: { id: kanjiId } });
        kanjis.push({
          literal: kanji.dataValues.literal,
          strokeCount: kanji.dataValues.strokeCount,
        });
      }

      return res.status(200).send({
        kanjis,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        err: err.toString(), //TODO add translation
      });
    });
};
