const _ = require('lodash');
const db = require('../../models');
const { Op } = require('sequelize');
const Kanji = db.Kanji;
const KanjiInList = db.KanjiInList;
const KanjiList = db.KanjiList;
const User = db.User;
const Radical = db.Radical;
const KanjiDicRef = db.KanjiDicRef;
const KanjiQueryCode = db.KanjiQueryCode;
const KanjiVariant = db.KanjiVariant;
const KanjiReading = db.KanjiReading;
const KanjiMeaning = db.KanjiMeaning;
const Nanori = db.Nanori;

function prepareKanjiList(listId, res) {
  return KanjiList.findOne({ where: { id: listId } }).then(async (list) => {
    let count = 0;
    let kanjis = [];

    await KanjiInList.findAll({
      where: { kanjiListId: list.id },
    }).then((result) => {
      kanjis = result.map((item) => item.kanjiId);
      count = result.length;
    });

    return res.status(200).send({
      list,
      kanjis,
      count,
    });
  });
}

exports.getKanjiLists = async (req, res) => {
  try {
    const lists = KanjiList.findAll({ where: { userId: req.userId } });

    lists
      .map(async (list) => {
        let count = 0;
        let kanjis = [];

        await KanjiInList.findAll({
          where: { kanjiListId: list.id },
        }).then((result) => {
          kanjis = result.map((item) => item.kanjiId);
          count = result.length;
        });

        return {
          list: list,
          kanjis,
          count,
        };
      })
      .then((lists) => {
        return res.status(200).json({
          lists: lists,
        });
      });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      error: err, //TODO add translation
    });
  }
};

exports.getKanjiList = (req, res) => {
  const { id } = req.params;
  KanjiList.findOne({
    where: { id: id },
    include: [
      {
        model: Kanji,
        include: [
          { model: Radical },
          { model: KanjiDicRef, as: 'kanjiDicRefs' },
          { model: KanjiQueryCode, as: 'kanjiQueryCodes' },
          { model: KanjiVariant, as: 'kanjiVariants' },
          { model: KanjiReading, as: 'kanjiReadings' },
          { model: KanjiMeaning, as: 'kanjiMeanings' },
          { model: Nanori, as: 'nanories' },
        ],
      },
    ],
  })
    .then((list) => {
      const dataValues = list.dataValues;
      return res.status(200).send({
        list: { ..._.omit(dataValues, 'Kanjis') },
        kanjis: dataValues.Kanjis,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.createKanjiList = (req, res) => {
  KanjiList.create(
    {
      name: req.body.name,
      userId: req.userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    { include: [User] }
  )
    .then((list) => {
      return res.status(200).send({
        list: list,
        kanjis: [],
        count: 0,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.updateKanjiList = async (req, res) => {
  await KanjiList.update(
    {
      name: req.body.name,
      updatedAt: new Date(),
    },
    {
      where: { id: req.body.id },
    }
  ).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  prepareKanjiList(req.body.id, res);
};

exports.deleteKanjiList = (req, res) => {
  const { id } = req.params;
  KanjiInList.destroy({ where: { kanjiListId: id } })
    .then(() => {
      KanjiList.destroy({ where: { id: id } })
        .then((list) => {
          return res.status(200).send();
        })
        .catch((err) => {
          return res.status(500).send({
            error: err.toString(), //TODO add translation
          });
        });
    })
    .catch((err) => {
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.addKanjiToList = async (req, res) => {
  const { id, kanjiId } = req.body;
  const kanji = await Kanji.findByPk(kanjiId).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (!kanji)
    return res.status(404).send({
      error: { name: 'Kanji not found' }, //TODO add translation
    });

  const kanjiList = await KanjiList.findByPk(id).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (!kanjiList)
    return res.status(404).send({
      error: { name: 'List not found' }, //TODO add translation
    });

  if (kanjiList.userId !== req.userId)
    return res.status(404).send({
      error: { name: "You can't change this list" }, //TODO add translation
    });

  const kanjiInList = await KanjiInList.findOne({
    where: {
      [Op.and]: [{ kanjiId: kanjiId }, { kanjiListId: id }],
    },
  }).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (kanjiInList) {
    return res.status(400).send({
      error: { name: 'This kanji is already in list' }, //TODO add translation
    });
  }

  await KanjiInList.create({
    kanjiId: kanjiId,
    kanjiListId: id,
    createdAt: new Date(),
    updatedAt: new Date(),
  }).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  prepareKanjiList(req.body.id, res);
};

exports.deleteKanjiFromList = async (req, res) => {
  const { id, kanjiId } = req.params;
  console.log(id, kanjiId);
  // TODO add checking user before delete
  KanjiInList.destroy({
    where: {
      [Op.and]: [{ kanjiId: kanjiId }, { kanjiListId: id }],
    },
  })
    .then(() => {
      return res.status(200).send();
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};
