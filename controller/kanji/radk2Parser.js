const fs = require('fs');
const db = require('../../models');
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;
const Radk = db.Radk;
const RadToKanji = db.RadToKanji;
const RadToRad = db.RadToRad;
const Kanji = db.Kanji;

function isInteger(str) {
  return Number.isInteger(Number.parseInt(str));
}

async function fillDBFromEntry(radEntry) {
  const hasTwoDigits = isInteger(radEntry[2]);
  const radical = await Radk.create({
    radical: radEntry[0],
    strokeCount: hasTwoDigits
      ? Number.parseInt(radEntry.slice(1, 3))
      : Number.parseInt(radEntry[1]),
  }).catch((err) => console.log(err));

  const start = hasTwoDigits ? 3 : 2;

  if (radical) {
    let kanji = await Kanji.findOne({
      where: { literal: radEntry[0] },
    }).catch((err) => console.log(err));

    if (kanji) {
      await RadToKanji.create({
        radId: radical.id,
        kanjiId: kanji.id,
      });
    }

    for (let i = start; i < radEntry.length; i++) {
      let kanji = await Kanji.findOne({
        where: { literal: radEntry[i] },
      }).catch((err) => console.log(err));

      if (kanji) {
        await RadToKanji.create({
          radId: radical.id,
          kanjiId: kanji.id,
        });
      }
    }
  }
}

exports.parseRadk2file = (req, res) => {
  console.log('start parse radk2');
  const path = process.cwd() + '\\JapDict\\krad\\radkfile2';
  fs.readFile(path, function (err, data) {
    if (err) return console.error('Error', err);

    let file = data;

    while (file.length > 0) {
      if (file.lastIndexOf('$') === -1) {
        file = '';
      } else {
        const radEntry = file
          .toString('utf8', file.lastIndexOf('$') + 1, file.length)
          .replace(/[ \n]/g, '');
        file = file.slice(0, file.lastIndexOf('$'));
        fillDBFromEntry(radEntry);
      }
    }
  });

  res.status(200).send();
};

exports.parseSetRadToRad = async (req, res) => {
  const rads = await Radk.findAll({
    include: [
      {
        model: Kanji,
        include: [{ model: Radk }],
      },
    ],
  });

  rads.forEach((radical) => {
    let linkedRads = new Set();
    radical.dataValues.Kanjis.forEach((kanji) => {
      kanji.Radks.forEach((linkedRad) => {
        if (linkedRad.id !== radical.id) linkedRads.add(linkedRad.id);
      });
    });
    const arrLinkedRads = Array.from(linkedRads);
    arrLinkedRads.forEach((linkedRadId) => {
      RadToRad.create({
        radId: radical.id,
        linkedRadId,
      });
    });
  });
  console.log('finish setting rads');
  res.status(200).send();
};
