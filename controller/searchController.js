var kuromoji = require('kuromoji');
const db = require('../models');
const config = require('../config/config.json');
const Sequelize = db.Sequelize;
const Op = Sequelize.Op;
const Kanji = db.Kanji;
const Radical = db.Radical;
const KanjiDicRef = db.KanjiDicRef;
const KanjiQueryCode = db.KanjiQueryCode;
const KanjiVariant = db.KanjiVariant;
const KanjiReading = db.KanjiReading;
const KanjiMeaning = db.KanjiMeaning;
const Nanori = db.Nanori;
const Abbreviation = db.Abbreviation;
const Priority = db.Priority;
const Word = db.Word;
const WordList = db.WordList;
const WordInList = db.WordInList;
const WordKanjiElement = db.WordKanjiElement;
const WordReadElement = db.WordReadElement;
const ReRestr = db.WordReRestr;
const WordSense = db.WordSense;
const Stagk = db.WordStagk;
const Stagr = db.WordStagr;
const Xref = db.WordXref;
const Ant = db.WordAnt;
const Pos = db.WordPos;
const Field = db.WordField;
const Misc = db.WordMisc;
const Dial = db.WordDial;
const LSource = db.WordLSource;
const Gloss = db.WordGloss;
const SInf = db.WordSInf;

const { isKanji, isRussian, isEnglish, hasKanji } = require('../utils');

async function searchKanji(text) {
  let result = [];
  for (let letter of text) {
    if (isKanji(letter)) {
      const kanji = await Kanji.findOne({
        where: { literal: letter },
        include: [
          { model: Radical },
          { model: KanjiDicRef, as: 'kanjiDicRefs' },
          { model: KanjiQueryCode, as: 'kanjiQueryCodes' },
          { model: KanjiVariant, as: 'kanjiVariants' },
          { model: KanjiReading, as: 'kanjiReadings' },
          { model: KanjiMeaning, as: 'kanjiMeanings' },
          { model: Nanori, as: 'nanories' },
        ],
      }).catch((err) => {
        console.log(err);
      });

      if (kanji) {
        const isNew = !result.some((item) => item.id === kanji.id);
        if (isNew) {
          result.push(kanji);
        }
      }
    }
  }
  return result;
}

async function searchKanjiIds(text) {
  let result = new Set();
  for (let letter of text) {
    if (isKanji(letter)) {
      const kanji = await Kanji.findOne({
        where: { literal: letter },
      }).catch((err) => {
        console.log(err);
      });

      if (kanji) {
        result.add(kanji.id);
      }
    }
  }
  return Array.from(result);
}

async function searchWord(text, type) {
  let wordIds = [];

  switch (type) {
    case 'keb': {
      const kanjiElements = await WordKanjiElement.findAll({
        where: {
          keb: text,
        },
      });

      kanjiElements.forEach((kEle) => {
        wordIds.push(kEle.wordId);
      });
      break;
    }
    case 'reb': {
      const readElements = await WordReadElement.findAll({
        where: {
          reb: text,
        },
      });

      readElements.forEach((rEle) => {
        wordIds.push(rEle.wordId);
      });
      break;
    }
    default: {
      const glosses = await Gloss.findAll({
        where: {
          data: {
            [Op.like]: text,
          },
        },
      });

      for (let gloss of glosses) {
        const sense = await WordSense.findByPk(gloss.senseId);
        wordIds.push(sense.wordId);
      }
      break;
    }
  }

  wordIds = Array.from(new Set(wordIds));

  let words = await Word.findAll({
    where: {
      id: {
        [Op.in]: wordIds,
      },
    },
    include: [
      {
        model: WordKanjiElement,
        as: 'kanjiElements',
        include: [{ model: Abbreviation }, { model: Priority }],
      },
      {
        model: WordReadElement,
        as: 'readElements',
        include: [
          { model: Abbreviation },
          { model: Priority },
          { model: ReRestr, as: 'reRestrs' },
        ],
      },
      {
        model: WordSense,
        as: 'senses',
        include: [
          { model: Stagk, as: 'stagks' },
          { model: Stagr, as: 'stagrs' },
          { model: Xref, as: 'xrefs' },
          { model: Ant, as: 'ants' },
          { model: Abbreviation, as: 'positions' },
          { model: Abbreviation, as: 'fields' },
          { model: Abbreviation, as: 'miscs' },
          { model: Abbreviation, as: 'dials' },
          { model: LSource, as: 'lsources' },
          { model: Gloss, as: 'glosses' },
          { model: SInf, as: 'sinfs' },
        ],
      },
    ],
    separate: true,
  });

  return words;
}

async function searchWordIds(text, type) {
  let wordIds = [];

  switch (type) {
    case 'keb': {
      const kanjiElements = await WordKanjiElement.findAll({
        where: {
          keb: text,
        },
      });

      kanjiElements.forEach((kEle) => {
        wordIds.push(kEle.wordId);
      });
      break;
    }
    case 'reb': {
      const readElements = await WordReadElement.findAll({
        where: {
          reb: text,
        },
      });

      readElements.forEach((rEle) => {
        wordIds.push(rEle.wordId);
      });
      break;
    }
    default: {
      const glosses = await Gloss.findAll({
        where: {
          data: {
            [Op.like]: text,
          },
        },
      });

      for (let gloss of glosses) {
        const sense = await WordSense.findByPk(gloss.senseId);
        wordIds.push(sense.wordId);
      }
      break;
    }
  }

  wordIds = Array.from(new Set(wordIds));

  return wordIds;
}

exports.search = async (req, res) => {
  const search = req.body.search;
  if (search) {
    kuromoji
      .builder({
        dicPath: process.cwd() + '/JapDict/kuromoji',
      })
      .build(async function (error, tokenizer) {
        const tokenized = await tokenizer.tokenize(search);
        let result = [];

        let searched = new Set();
        for (let word of tokenized) {
          if (
            !searched.has(word.basic_form) &&
            !searched.has(word.surface_form)
          ) {
            const kanjis = await searchKanji(word.basic_form);
            let words = [];
            if (word.word_type !== 'UNKNOWN') {
              words = await searchWord(
                word.basic_form,
                hasKanji(word.basic_form) ? 'keb' : 'reb'
              );
              searched.add(word.basic_form);
            } else {
              words = await searchWord(word.surface_form, 'gloss');
              searched.add(word.surface_form);
            }
            result.push({ word: word, words, kanjis });
          }
        }

        res.status(200).send(result);
      });
  } else {
    res.status(200).send({
      words: [],
      kanjis: [],
    });
  }
};

exports.reader = async (req, res) => {
  const text = req.body.text;
  if (text) {
    kuromoji
      .builder({
        dicPath: process.cwd() + '/JapDict/kuromoji',
      })
      .build(async function (error, tokenizer) {
        const tokenized = await tokenizer.tokenize(text);
        let result = [];
        for (let word of tokenized) {
          const kanjis = await searchKanjiIds(word.basic_form);
          let words = [];
          if (word.word_type !== 'UNKNOWN') {
            words = await searchWordIds(
              word.basic_form,
              hasKanji(word.basic_form) ? 'keb' : 'reb'
            );
          } else {
            words = await searchWordIds(word.surface_form, 'gloss');
          }
          result.push({ word: word, words, kanjis });
        }

        res.status(200).send(result);
      });
  } else {
    res.status(200).send({
      words: [],
      kanjis: [],
    });
  }
};
