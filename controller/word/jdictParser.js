const fs = require('fs');
const xml2js = require('xml2js');
const db = require('../../models');
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;
const KanjiElement = db.WordKanjiElement;
const KeInf = db.WordKeInf;
const KePri = db.WordKePri;
const Abbreviation = db.Abbreviation;
const Priority = db.Priority;
const Word = db.Word;
const ReadElement = db.WordReadElement;
const ReInf = db.WordReInf;
const RePri = db.WordRePri;
const Sense = db.WordSense;
const Stagk = db.WordStagk;
const Stagr = db.WordStagr;
const Xref = db.WordXref;
const Ant = db.WordAnt;
const Pos = db.WordPos;
const Field = db.WordField;
const Misc = db.WordMisc;
const LSource = db.WordLSource;
const Dial = db.WordDial;
const Gloss = db.WordGloss;
const SInf = db.WordSInf;
const ReRestr = db.WordReRestr;
const { isInt, hasKanji } = require('../../utils');

function prepareAbbreviation(abbr) {
    let result =
        abbr.indexOf('&') !== -1 ? abbr.slice(abbr.indexOf('&') + 1) : abbr;
    result =
        abbr.indexOf(';') !== -1
            ? result.slice(0, result.indexOf(';'))
            : result;
    return result;
}

async function addWord(word) {
    return await sequelize.transaction(async (t) => {
        return await Word.create(
            {
                entSeq: word,
            },
            { transaction: t }
        );
    });
}

async function addKElement(kElement, word) {
    return await sequelize.transaction(async (t) => {
        return await KanjiElement.create(
            {
                keb: kElement,
                wordId: word.id,
            },
            { transaction: t }
        );
    });
}

async function addKeInf(keInf, kElement) {
    const abbr = await sequelize.transaction(async (t) => {
        return await Abbreviation.findOne({
            where: {
                abbr: prepareAbbreviation(keInf),
            },
            transaction: t,
        });
    });

    if (abbr) {
        return await sequelize.transaction(async (t) => {
            return await KeInf.create(
                {
                    kebId: kElement.id,
                    abbrId: abbr.id,
                },
                { transaction: t }
            ).catch(() => {});
        });
    } else {
        return Promise.resolve();
    }
}

async function fillPriorities(entries) {
    let priorities = [];
    entries &&
        entries.forEach((entry) => {
            if (entry.K_ELE && entry.K_ELE.length > 0) {
                entry.K_ELE.forEach((kEle) => {
                    if (kEle.KE_PRI && kEle.KE_PRI.length > 0) {
                        kEle.KE_PRI.forEach((priority) => {
                            if (!priorities.includes(priority))
                                priorities.push(priority);
                        });
                    }
                });
            }
            if (entry.R_ELE && entry.R_ELE.length > 0) {
                entry.R_ELE.forEach((rEle) => {
                    if (rEle.RE_PRI && rEle.RE_PRI.length > 0) {
                        rEle.RE_PRI.forEach((priority) => {
                            if (!priorities.includes(priority))
                                priorities.push(priority);
                        });
                    }
                });
            }
        });
    return await sequelize.transaction(async (t) => {
        return await Priority.bulkCreate(
            priorities.map((pri) => ({ pri })),
            { transaction: t }
        ).catch(() => {});
    });
}

async function addKePri(kePri, kElement) {
    let pri = await sequelize.transaction(async (t) => {
        return await Priority.findOne({
            where: {
                pri: kePri,
            },
            transaction: t,
        });
    });

    if (pri) {
        return await sequelize.transaction(async (t) => {
            return await KePri.create(
                {
                    kebId: kElement.id,
                    priId: pri.id,
                },
                { transaction: t }
            );
        });
    } else {
        return Promise.resolve();
    }
}

async function parseKanjiElement(kEle, word) {
    const kElement = await addKElement(kEle.KEB[0], word);

    if (kEle.KE_INF && kEle.KE_INF.length > 0) {
        for (let i = 0; i < kEle.KE_INF.length; i++) {
            addKeInf(kEle.KE_INF[i], kElement);
        }
    }

    if (kEle.KE_PRI && kEle.KE_PRI.length > 0) {
        for (let i = 0; i < kEle.KE_PRI.length; i++) {
            addKePri(kEle.KE_PRI[i], kElement);
        }
    }

    return Promise.resolve();
}

async function addRElement(reb, reNoKanji, reRestr, word) {
    const { ReRestr } = sequelize.models;
    return await sequelize.transaction(async (t) => {
        return await ReadElement.create(
            {
                reb,
                reNoKanji,
                wordId: word.id,
                reRestrs: reRestr,
            },
            {
                include: ['reRestrs'],
                transaction: t,
            }
        );
    });
}

async function addReInf(reInf, rElement) {
    const abbr = await sequelize.transaction(async (t) => {
        return await Abbreviation.findOne({
            where: {
                abbr: prepareAbbreviation(reInf),
            },
            transaction: t,
        });
    });

    if (abbr) {
        return await sequelize.transaction(async (t) => {
            return await ReInf.create(
                {
                    rebId: rElement.id,
                    abbrId: abbr.id,
                },
                { transaction: t }
            ).catch(() => {});
        });
    } else {
        return Promise.resolve();
    }
}

async function addRePri(rePri, rElement) {
    let pri = await sequelize.transaction(async (t) => {
        return await Priority.findOne({
            where: {
                pri: rePri,
            },
            transaction: t,
        });
    });

    if (pri) {
        return await sequelize.transaction(async (t) => {
            return await RePri.create(
                {
                    rebId: rElement.id,
                    priId: pri.id,
                },
                { transaction: t }
            );
        });
    } else {
        return Promise.resolve();
    }
}

async function parseReadElement(rEle, word) {
    const noKanji = Boolean(rEle.RE_NOKANJI);
    const reRestr =
        rEle.RE_RESTR && rEle.RE_RESTR.length > 0
            ? rEle.RE_RESTR.map((restr) => ({ data: restr }))
            : [];
    const rElement = await addRElement(rEle.REB[0], noKanji, reRestr, word);

    if (rEle.RE_INF && rEle.RE_INF.length > 0) {
        for (let i = 0; i < rEle.RE_INF.length; i++) {
            addReInf(rEle.RE_INF[i], rElement);
        }
    }

    if (rEle.RE_PRI && rEle.RE_PRI.length > 0) {
        for (let i = 0; i < rEle.RE_PRI.length; i++) {
            addRePri(rEle.RE_PRI[i], rElement);
        }
    }

    return Promise.resolve();
}

async function addSense(
    { stagk, stagr, xref, ant, lsource, gloss, sInf },
    word
) {
    return await sequelize.transaction(async (t) => {
        return await Sense.create(
            {
                stagks: stagk,
                stagrs: stagr,
                xrefs: xref,
                ants: ant,
                lsources: lsource,
                glosses: gloss,
                sinfs: sInf,
                wordId: word.id,
            },
            {
                include: [
                    'stagks',
                    'stagrs',
                    'xrefs',
                    'ants',
                    'lsources',
                    'glosses',
                    'sinfs',
                ],
                transaction: t,
            }
        );
    });
}

function setXref(data) {
    let keb = null;
    let reb = null;
    let numSense = -1;
    const xref = data.split('・');
    xref.forEach((item) => {
        if (isInt(item)) {
            numSense = Number.parseInt(item);
        } else if (hasKanji(item)) {
            keb = item;
        } else {
            reb = item;
        }
    });
    return { keb, reb, numSense };
}

function setLSource(lsource) {
    let lang = 'eng';
    let data = null;
    let lsWasei = null;
    let lsType = 'full';
    if (typeof lsource === 'object') {
        lang =
            lsource['$'] && lsource['$']['XML:LANG']
                ? lsource['$']['XML:LANG']
                : lang;
        lsWasei =
            lsource['$'] && lsource['$']['LS_WASEI']
                ? lsource['$']['LS_WASEI']
                : lsWasei;
        lsType =
            lsource['$'] && lsource['$']['LS_TYPE']
                ? lsource['$']['LS_TYPE']
                : lsType;
        data = lsource['_'] ? lsource['_'] : data;
    } else {
        data = lsource || null;
    }
    return {
        lang,
        data,
        lsWasei,
        lsType,
    };
}

function setGloss(gloss) {
    let lang = 'eng';
    let data = null;
    let type = null;
    let source = 'jmdict';
    if (typeof gloss === 'object') {
        lang =
            gloss['$'] && gloss['$']['XML:LANG']
                ? gloss['$']['XML:LANG']
                : lang;
        type = gloss['$'] && gloss['$']['G_TYPE'] ? gloss['$']['G_TYPE'] : type;
        data = gloss['_'] ? gloss['_'] : data;
    } else {
        data = gloss || null;
    }
    return {
        lang,
        data,
        type,
        source,
    };
}

async function addToAbbr(inf, sense, type) {
    let mod = null;
    switch (type) {
        case 'pos':
            mod = Pos;
        case 'field':
            mod = Field;
        case 'misc':
            mod = Misc;
        case 'dial':
            mod = Dial;
    }
    const abbr = await sequelize.transaction(async (t) => {
        return await Abbreviation.findOne({
            where: {
                abbr: prepareAbbreviation(inf),
            },
            transaction: t,
        });
    });

    if (abbr) {
        return await sequelize.transaction(async (t) => {
            return await mod
                .create(
                    {
                        senseId: sense.id,
                        abbrId: abbr.id,
                    },
                    { transaction: t }
                )
                .catch(() => {});
        });
    } else {
        return Promise.resolve();
    }
}

async function parseSense(sense, word) {
    const stagk =
        (sense.STAGK &&
            sense.STAGK.length > 0 &&
            sense.STAGK.map((data) => ({ data }))) ||
        [];
    const stagr =
        (sense.STAGR &&
            sense.STAGR.length > 0 &&
            sense.STAGR.map((data) => ({ data }))) ||
        [];
    const xref =
        (sense.XREF &&
            sense.XREF.length > 0 &&
            sense.XREF.map((data) => setXref(data))) ||
        [];
    const ant =
        (sense.ANT &&
            sense.ANT.length > 0 &&
            sense.ANT.map((data) => setXref(data))) ||
        [];

    const lsource =
        (sense.LSOURCE &&
            sense.LSOURCE.length > 0 &&
            sense.LSOURCE.map((data) => setLSource(data))) ||
        [];

    const gloss =
        (sense.GLOSS &&
            sense.GLOSS.length > 0 &&
            sense.GLOSS.map((data) => setGloss(data))) ||
        [];

    const sInf =
        (sense.S_INF &&
            sense.S_INF.length > 0 &&
            sense.S_INF.map((data) => ({ data }))) ||
        [];

    const wordSense = await addSense(
        { stagk, stagr, xref, ant, lsource, gloss, sInf },
        word
    );

    if (sense.POS && sense.POS.length > 0) {
        for (let i = 0; i < sense.POS.length; i++) {
            addToAbbr(sense.POS[i], wordSense, 'pos');
        }
    }

    if (sense.FIELD && sense.FIELD.length > 0) {
        for (let i = 0; i < sense.FIELD.length; i++) {
            addToAbbr(sense.FIELD[i], wordSense, 'field');
        }
    }

    if (sense.MISC && sense.MISC.length > 0) {
        for (let i = 0; i < sense.MISC.length; i++) {
            addToAbbr(sense.MISC[i], wordSense, 'misc');
        }
    }

    if (sense.DIAL && sense.DIAL.length > 0) {
        for (let i = 0; i < sense.DIAL.length; i++) {
            addToAbbr(sense.DIAL[i], wordSense, 'dial');
        }
    }

    return Promise.resolve();
}

async function parseEntry(entry) {
    const word = await addWord(entry.ENT_SEQ[0]);

    if (entry.K_ELE && entry.K_ELE.length > 0) {
        for (let i = 0; i < entry.K_ELE.length; i++) {
            parseKanjiElement(entry.K_ELE[i], word);
        }
    }

    if (entry.R_ELE && entry.R_ELE.length > 0) {
        for (let i = 0; i < entry.R_ELE.length; i++) {
            parseReadElement(entry.R_ELE[i], word);
        }
    }

    if (entry.SENSE && entry.SENSE.length > 0) {
        for (let i = 0; i < entry.SENSE.length; i++) {
            parseSense(entry.SENSE[i], word);
        }
    }

    return Promise.resolve();
}

exports.parseJMdict = (req, res) => {
    console.log('start parse JMdict');
    const parser = new xml2js.Parser({ strict: false });
    const path = process.cwd() + '\\JapDict\\JMdict\\JMdict';
    fs.readFile(path, function (err, data) {
        if (err) return console.error('Error', err);
        parser.parseStringPromise(data).then(async (result) => {
            const {
                JMDICT: { ENTRY },
            } = result;

            await fillPriorities(ENTRY);

            for (let i = 0; i < ENTRY.length; i++) {
                await parseEntry(ENTRY[i]);
            }
        });
    });

    res.status(200).send();
};
