const _ = require('lodash');
const db = require('../../models');
const { Op } = require('sequelize');
const User = db.User;
const Abbreviation = db.Abbreviation;
const Priority = db.Priority;
const Word = db.Word;
const WordList = db.WordList;
const WordInList = db.WordInList;
const WordKanjiElement = db.WordKanjiElement;
const WordReadElement = db.WordReadElement;
const ReRestr = db.WordReRestr;
const WordSense = db.WordSense;
const Stagk = db.WordStagk;
const Stagr = db.WordStagr;
const Xref = db.WordXref;
const Ant = db.WordAnt;
const Pos = db.WordPos;
const Field = db.WordField;
const Misc = db.WordMisc;
const Dial = db.WordDial;
const LSource = db.WordLSource;
const Gloss = db.WordGloss;
const SInf = db.WordSInf;

/**
 * Function for getting words ids from list and count of them as structure
 * @param {number} listId - id of words list
 * @returns {{list: WordList, words: Array<Word>, count: Number}} Return structure of result data if all success
 * @returns {{ error: Error }} Return error if catch error
 */

async function prepareWordList(listId) {
  const list = await WordList.findOne({
    where: { id: listId },
  }).catch((err) => ({ error: err }));

  let count = 0;
  let words = [];

  if (!list) return { error: `Can't find list` };

  const wordLists = await WordInList.findAll({
    where: { wordListId: list.id },
  }).catch((err) => ({ error: err }));

  if (wordLists) {
    words = wordLists.map((item) => item.wordId);
    count = wordLists.length;
  }

  return {
    list,
    words,
    count,
  };
}

exports.getWordLists = async (req, res) => {
  await WordList.findAll({
    where: { userId: req.userId },
    include: [
      {
        model: Word,
        include: [
          {
            model: WordKanjiElement,
            as: 'kanjiElements',
            include: [{ model: Abbreviation }, { model: Priority }],
          },
          {
            model: WordReadElement,
            as: 'readElements',
            include: [
              { model: Abbreviation },
              { model: Priority },
              { model: ReRestr, as: 'reRestrs' },
            ],
          },
          {
            model: WordSense,
            as: 'senses',
            include: [
              { model: Stagk, as: 'stagks' },
              { model: Stagr, as: 'stagrs' },
              { model: Xref, as: 'xrefs' },
              { model: Ant, as: 'ants' },
              { model: Abbreviation, as: 'positions' },
              { model: Abbreviation, as: 'fields' },
              { model: Abbreviation, as: 'miscs' },
              { model: Abbreviation, as: 'dials' },
              { model: LSource, as: 'lsources' },
              { model: Gloss, as: 'glosses' },
              { model: SInf, as: 'sinfs' },
            ],
          },
        ],
      },
    ],
  })
    .then((lists) => {
      const resLists = lists.map((list) => {
        const dataValues = list.dataValues;
        return {
          list: { ..._.omit(dataValues, 'Words') },
          words: dataValues.Words,
          count: dataValues.Words.length,
        };
      });
      return res.status(200).send(resLists);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        error: err.toString(), //TODO add translation
      });
    });

  // lists
  //   .map(async (list) => {
  //     let count = 0;
  //     let words = [];

  //     await WordInList.findAll({
  //       where: { wordListId: list.id },
  //     }).then((result) => {
  //       words = result.map((item) => item.wordId);
  //       count = result.length;
  //     });

  //     return {
  //       list: list,
  //       words,
  //       count,
  //     };
  //   })
  //   .then((lists) => {
  //     return res.status(200).json({
  //       lists: lists,
  //     });
  //   });
};

exports.createWordPos = async (req, res) => {
  const wordPos = await Pos.create({ senseId: 51, abbrId: 14 });
  return res.status(200).send(wordPos);
};

exports.getWordList = (req, res) => {
  const { id } = req.params;
  WordList.findOne({
    where: { id: id }, //TODO Create word select function
    include: [
      {
        model: Word,
        include: [
          {
            model: WordKanjiElement,
            as: 'kanjiElements',
            include: [{ model: Abbreviation }, { model: Priority }],
          },
          {
            model: WordReadElement,
            as: 'readElements',
            include: [
              { model: Abbreviation },
              { model: Priority },
              { model: ReRestr, as: 'reRestrs' },
            ],
          },
          {
            model: WordSense,
            as: 'senses',
            include: [
              { model: Stagk, as: 'stagks' },
              { model: Stagr, as: 'stagrs' },
              { model: Xref, as: 'xrefs' },
              { model: Ant, as: 'ants' },
              { model: Abbreviation, as: 'positions' },
              { model: Abbreviation, as: 'fields' },
              { model: Abbreviation, as: 'miscs' },
              { model: Abbreviation, as: 'dials' },
              { model: LSource, as: 'lsources' },
              { model: Gloss, as: 'glosses' },
              { model: SInf, as: 'sinfs' },
            ],
          },
        ],
      },
    ],
  })
    .then((list) => {
      const dataValues = list.dataValues;
      return res.status(200).send({
        list: { ..._.omit(dataValues, 'Words') },
        words: dataValues.Words,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.createWordList = (req, res) => {
  WordList.create(
    {
      name: req.body.name,
      userId: req.userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    { include: [User] }
  )
    .then((list) => {
      return res.status(200).send({
        list: list,
        words: [],
        count: 0,
      });
    })
    .catch((err) => {
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.updateWordList = async (req, res) => {
  const { id } = req.params;
  await WordList.update(
    {
      name: req.body.name,
      updatedAt: new Date(),
    },
    {
      where: { id },
    }
  ).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  const result = await prepareWordList(id);
  if (result && !result.error) {
    res.status(200).send(result);
  } else {
    res.status(500).send(result);
  }
};

exports.deleteWordList = (req, res) => {
  const { id } = req.params;

  WordList.destroy({ where: { id: id } })
    .then(() => {
      return res.status(200).send();
    })
    .catch((err) => {
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};

exports.addWordToList = async (req, res) => {
  const { id, wordId } = req.params;
  const word = await Word.findByPk(wordId).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (!word)
    return res.status(404).send({
      error: { name: 'Word not found' }, //TODO add translation
    });

  const wordList = await WordList.findByPk(id).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (!wordList)
    return res.status(404).send({
      error: { name: 'List not found' }, //TODO add translation
    });

  if (wordList.userId !== req.userId)
    return res.status(404).send({
      error: { name: "You can't change this list" }, //TODO add translation
    });

  const wordInList = await WordInList.findOne({
    where: {
      [Op.and]: [{ wordId }, { wordListId: id }],
    },
  }).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  if (wordInList) {
    return res.status(400).send({
      error: { name: 'This word is already in list' }, //TODO add translation
    });
  }

  await WordInList.create({
    wordId: wordId,
    wordListId: id,
    createdAt: new Date(),
    updatedAt: new Date(),
  }).catch((err) => {
    return res.status(500).send({
      error: err.toString(), //TODO add translation
    });
  });

  const result = await prepareWordList(req.params.id);

  if (result && !result.error) {
    res.status(200).send(result);
  } else {
    res.status(500).send(result);
  }
};

exports.deleteWordFromList = async (req, res) => {
  const { id, wordId } = req.params;
  // TODO add checking user before delete
  WordInList.destroy({
    where: {
      [Op.and]: [{ wordId }, { wordListId: id }],
    },
  })
    .then(() => {
      return res.status(200).send();
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send({
        error: err.toString(), //TODO add translation
      });
    });
};
