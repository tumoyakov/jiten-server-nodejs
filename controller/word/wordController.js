const _ = require('lodash');
const db = require('../../models');
const {
  Word,
  WordKanjiElement,
  WordKeInf,
  WordKePri,
  WordReadElement,
  WordRePri,
  WordReInf,
  WordReRestr,
  WordSense,
  WordStagk,
  WordStagr,
  WordXref,
  WordAnt,
  WordField,
  WordPos,
  WordMisc,
  WordLSource,
  WordGloss,
  WordSInf,
  Abbreviation,
  Priority,
} = db;
const { Op } = db.Sequelize;

exports.getWord = async (req, res) => {
  const { id } = req.params;
  const word = (
    await Word.findOne({
      where: { id: id },
      include: [
        {
          model: WordKanjiElement,
          as: 'kanjiElements',
          include: [{ model: Abbreviation }, { model: Priority }],
        },
        {
          model: WordReadElement,
          as: 'readElements',
          include: [
            { model: Abbreviation },
            { model: Priority },
            { model: WordReRestr, as: 'reRestrs' },
          ],
        },
        {
          model: WordSense,
          as: 'senses',
          include: [
            { model: WordStagk, as: 'stagks' },
            { model: WordStagr, as: 'stagrs' },
            { model: WordXref, as: 'xrefs' },
            { model: WordAnt, as: 'ants' },
            { model: Abbreviation, as: 'positions' },
            { model: Abbreviation, as: 'fields' },
            { model: Abbreviation, as: 'miscs' },
            { model: Abbreviation, as: 'dials' },
            { model: WordLSource, as: 'lsources' },
            { model: WordGloss, as: 'glosses' },
            { model: WordSInf, as: 'sinfs' },
          ],
        },
      ],
    }).catch((err) => {
      return res.status(500).send(err);
    })
  ).dataValues;
  res.status(200).send({ ...word });
};

exports.getWords = async (req, res) => {
  const { ids } = req.body;
  await Word.findAll({
    where: { id: { [Op.in]: ids } },
    include: [
      {
        model: WordKanjiElement,
        as: 'kanjiElements',
        include: [{ model: Abbreviation }, { model: Priority }],
      },
      {
        model: WordReadElement,
        as: 'readElements',
        include: [
          { model: Abbreviation },
          { model: Priority },
          { model: WordReRestr, as: 'reRestrs' },
        ],
      },
      {
        model: WordSense,
        as: 'senses',
        include: [
          { model: WordStagk, as: 'stagks' },
          { model: WordStagr, as: 'stagrs' },
          { model: WordXref, as: 'xrefs' },
          { model: WordAnt, as: 'ants' },
          { model: Abbreviation, as: 'positions' },
          { model: Abbreviation, as: 'fields' },
          { model: Abbreviation, as: 'miscs' },
          { model: Abbreviation, as: 'dials' },
          { model: WordLSource, as: 'lsources' },
          { model: WordGloss, as: 'glosses' },
          { model: WordSInf, as: 'sinfs' },
        ],
      },
    ],
  })
    .then((words) => {
      res.status(200).send({ words });
    })
    .catch((err) => {
      return res.status(500).send(err);
    });
};
