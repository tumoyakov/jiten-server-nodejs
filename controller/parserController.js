const fs = require('fs');
const xml2js = require('xml2js');
const db = require('../models');
const jdictParser = require('./word/jdictParser');
const radk2Parser = require('./kanji/radk2Parser');
const Kanji = db.Kanji;

exports.parseKanjidic = (req, res) => {
  console.log('Start parse KanjiDic');
  const parser = new xml2js.Parser();
  const path = process.cwd() + '\\JapDict\\KanjiDic\\kanjidic2.xml';
  fs.readFile(path, function (err, data) {
    if (err) console.error('Error: ', err);
    console.log('data', data);
    parser
      .parseStringPromise(data)
      .then(function (result) {
        const {
          kanjidic2: { header, character },
        } = result;
        character.forEach((item) => {
          const {
            literal,
            codepoint,
            radical,
            misc,
            dic_number,
            query_code,
            reading_meaning,
          } = item;

          const r_codepoint = codepoint
            ? codepoint[0].cp_value.find(
                (cp) => cp && cp['$'] && cp['$'].cp_type === 'ucs'
              )._
            : null;
          const grade = misc[0].grade ? misc[0].grade[0] : null;
          const strokeCount = misc[0].stroke_count
            ? Number(misc[0].stroke_count[0])
            : null;
          const freq = misc[0].freq ? Number(misc[0].freq[0]) : null;
          const jlpt = misc[0].jlpt ? Number(misc[0].jlpt[0]) : null;

          const r_radical = radical
            ? radical[0].rad_value.find(
                (rad) => rad && rad['$'] && rad['$'].rad_type === 'classical'
              )._
            : null;

          const kanjiVariants = misc[0].variant
            ? misc[0].variant.map((i) => {
                return {
                  code: i._,
                  type: i['$'].var_type,
                };
              })
            : null;

          const kanjiDicRefs =
            dic_number && dic_number[0].dic_ref
              ? dic_number[0].dic_ref.map((i) => {
                  return {
                    code: i._,
                    type: i['$'].dr_type,
                  };
                })
              : null;

          const kanjiQueryCodes =
            query_code && query_code[0].q_code
              ? query_code[0].q_code.map((i) => {
                  return {
                    code: i._,
                    type: i['$'].qc_type,
                  };
                })
              : null;

          const kanjiReadings =
            reading_meaning &&
            reading_meaning[0].rmgroup &&
            reading_meaning[0].rmgroup[0] &&
            reading_meaning[0].rmgroup[0].reading
              ? reading_meaning[0].rmgroup[0].reading
                  .filter((i) => {
                    return (
                      i['$'].r_type === 'ja_on' || i['$'].r_type === 'ja_kun'
                    );
                  })
                  .map((i) => {
                    return {
                      reading: i._,
                      type: i['$'].r_type,
                    };
                  })
              : null;

          const kanjiMeanings =
            reading_meaning &&
            reading_meaning[0].rmgroup &&
            reading_meaning[0].rmgroup[0] &&
            reading_meaning[0].rmgroup[0].meaning
              ? reading_meaning[0].rmgroup[0].meaning
                  .filter((i) => {
                    if (typeof i !== 'object') return true;
                    return !i['$'] || !i['$'].m_lang || i['$'].m_lang === 'ru';
                  })
                  .map((i) => {
                    return {
                      meaning: typeof i !== 'object' ? i : i._,
                      lang: i['$'] && i['$'].m_lang ? i['$'].m_lang : 'eng',
                      source: 'kanjidic2',
                    };
                  })
              : null;

          const nanories =
            reading_meaning && reading_meaning[0].nanori
              ? reading_meaning[0].nanori.map((i) => {
                  return {
                    nanori: i,
                  };
                })
              : null;

          const kanji = {
            literal: literal[0],
            codepoint: r_codepoint,
            radicalId: Number(r_radical),
            grade: grade,
            strokeCount: strokeCount,
            freq: freq,
            jlpt: jlpt,
            kanjiVariants: kanjiVariants,
            kanjiDicRefs: kanjiDicRefs,
            kanjiQueryCodes: kanjiQueryCodes,
            kanjiReadings: kanjiReadings,
            kanjiMeanings: kanjiMeanings,
            nanories: nanories,
          };

          Kanji.create(
            {
              ...kanji,
            },
            {
              include: [
                'kanjiVariants',
                'kanjiDicRefs',
                'kanjiQueryCodes',
                'kanjiReadings',
                'kanjiMeanings',
                'nanories',
              ],
            }
          );
        });
        //console.log(Kanji);
        console.log('all done');
      })
      .catch((err) => {
        console.error('Error: ', err);
      });
  });

  res.status(200).send();
};

exports.parseJMdict = (req, res) => {
  jdictParser.parseJMdict(req, res);
};

exports.parseRadk = (req, res) => {
  radk2Parser.parseRadk2file(req, res);
};

exports.linkRadks = (req, res) => {
  radk2Parser.parseSetRadToRad(req, res);
};
