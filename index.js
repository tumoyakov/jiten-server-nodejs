const express = require('express');
const app = express();
const fs = require('fs');
const https = require('https');
const cors = require('cors');
const bodyParser = require('body-parser');

if (process.stdout._handle) process.stdout._handle.setBlocking(true);

function logger(req, res, next) {
  console.log('%s %s', req.method, req.url);
  next();
}

var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', '*');
  next();
};

const httpsOptions = {
  key: fs.readFileSync('/etc/letsencrypt/live/jiten-read.host/privkey.pem'), // путь к ключу
  cert: fs.readFileSync('/etc/letsencrypt/live/jiten-read.host/fullchain.pem'), // путь к сертификату
};

app.use(cors());
app.use(logger);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(allowCrossDomain);
require('./routes')(app);

https.createServer(httpsOptions, app).listen(443);
