
const alphabet = require("./alphabet");
const kanjiUtils = require("./kanjiUtils");

module.exports = {
    ...alphabet,
    ...kanjiUtils
}