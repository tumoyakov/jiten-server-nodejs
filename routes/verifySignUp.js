const db = require('../models');
const User = db.User;

const checkDuplicate = (req, res, next) => {
    console.log(req.body);
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (user) {
            res.status(400).send('User with this email already registered');
            return;
        }

        User.findOne({
            where: {
                login: req.body.login
            }
        }).then(user => {
            if (user) {
                res.status(400).send('User with this login already registered');
                return;
            }

            next();
        });
    });
};

const signUpVerify = {};
signUpVerify.checkDuplicate = checkDuplicate;

module.exports = signUpVerify;
