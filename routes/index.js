const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');

module.exports = function (app) {
  const userController = require('../controller/userController');
  const parserController = require('../controller/parserController');
  const kanjiListController = require('../controller/kanji/kanjiListController');
  const kanjiController = require('../controller/kanji/kanjiController');
  const wordController = require('../controller/word/wordController');
  const wordListController = require('../controller/word/wordListController');
  const searchController = require('../controller/searchController');
  const testController = require('../controller/testController');

  //Profile
  app.get('/user/me', [authJwt.verifyToken], userController.userContent);
  app.post('/signup', [verifySignUp.checkDuplicate], userController.signup);
  app.post('/signin', userController.signin);

  //Parser
  app.post('/parse/kanjidic/', parserController.parseKanjidic);
  app.post('/parse/jmdict', parserController.parseJMdict);
  app.post('/parse/radk/', parserController.parseRadk);
  app.post('/link/radk/', parserController.linkRadks);

  //KanjiList
  app.get(
    '/kanji/lists',
    [authJwt.verifyToken],
    kanjiListController.getKanjiLists
  );
  app.get(
    '/kanji/list/:id',
    [authJwt.verifyToken],
    kanjiListController.getKanjiList
  );
  app.post(
    '/kanji/list/create',
    [authJwt.verifyToken],
    kanjiListController.createKanjiList
  );
  app.post(
    '/kanji/list/:id',
    [authJwt.verifyToken],
    kanjiListController.updateKanjiList
  );
  app.delete(
    '/kanji/list/:id',
    [authJwt.verifyToken],
    kanjiListController.deleteKanjiList
  );

  //KanjiInList
  app.post(
    '/add/kanji/',
    [authJwt.verifyToken],
    kanjiListController.addKanjiToList
  );
  app.delete(
    '/delete/kanji/:id/:kanjiId',
    [authJwt.verifyToken],
    kanjiListController.deleteKanjiFromList
  );

  //Kanji
  app.get('/kanji/item/:id', kanjiController.getKanji);
  app.get('/kanji/radk/', kanjiController.getRadk);
  app.post('/kanji/find-by-rads', kanjiController.findKanjisByRads);
  app.post('/kanjis/get', kanjiController.getKanjis);

  //Word
  app.get('/word/item/:id', wordController.getWord);
  app.post('/words/get', wordController.getWords);

  //WordList
  app.get(
    '/word/lists',
    [authJwt.verifyToken],
    wordListController.getWordLists
  );
  app.get(
    '/word/list/:id',
    [authJwt.verifyToken],
    wordListController.getWordList
  );
  app.post(
    '/word/list/create',
    [authJwt.verifyToken],
    wordListController.createWordList
  );
  app.post(
    '/word/list/:id',
    [authJwt.verifyToken],
    wordListController.updateWordList
  );
  app.delete(
    '/word/list/:id',
    [authJwt.verifyToken],
    wordListController.deleteWordList
  );

  //WordInList
  app.post(
    '/word/list/:id/:wordId',
    [authJwt.verifyToken],
    wordListController.addWordToList
  );
  app.delete(
    '/word/list/:id/:wordId',
    [authJwt.verifyToken],
    wordListController.deleteWordFromList
  );

  //Search
  app.post('/search', searchController.search);
  app.post('/reader', searchController.reader);

  //Statistic
  app.post('/statistic', [authJwt.verifyToken], testController.setStatistic);

  //dev
  app.post('/create/wordpos', wordListController.createWordPos);
};
