'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordGloss = sequelize.define(
        'WordGloss',
        {
            lang: DataTypes.STRING,
            data: DataTypes.STRING,
            source: DataTypes.STRING,
            type: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordGloss.associate = function (models) {
        WordGloss.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordGloss;
};
