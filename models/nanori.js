'use strict';
module.exports = (sequelize, DataTypes) => {
    const Nanori = sequelize.define(
        'Nanori',
        {
            nanori: DataTypes.STRING
        },
        {timestamps: false}
    );
    Nanori.associate = function(models) {
        Nanori.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return Nanori;
};