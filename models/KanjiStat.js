'use strict';
module.exports = (sequelize, DataTypes) => {
  const KanjiStat = sequelize.define('KanjiStat', {
    attempts: DataTypes.TEXT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
  KanjiStat.associate = function (models) {
    KanjiStat.belongsTo(models.User, {
      foreignKey: 'userId',
    });
    KanjiStat.belongsTo(models.Kanji, {
      foreignKey: 'kanjiId',
    });
  };
  return KanjiStat;
};
