'use strict';

module.exports = (sequelize, DataTypes) => {
    const KanjiList = sequelize.define('KanjiList', {
        name: DataTypes.STRING,
        userId: DataTypes.INTEGER,
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    }, {});
    KanjiList.associate = function (models) {
        const {User, Kanji} = models;
        KanjiList.belongsTo(User,
            {
                foreignKey: 'userId'
            });
        KanjiList.belongsToMany(Kanji,
            {
                through: 'KanjiInLists',
                foreignKey: 'kanjiListId',
                otherKey: 'kanjiId'
            });
    };
    return KanjiList;
};
