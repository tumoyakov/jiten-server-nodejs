'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordRePri = sequelize.define(
        'WordRePri',
        {
            rebId: DataTypes.INTEGER,
            priId: DataTypes.INTEGER,
        },
        { timestamps: false }
    );
    return WordRePri;
};
