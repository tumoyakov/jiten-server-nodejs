'use strict';
module.exports = (sequelize, DataTypes) => {
  const WordListStat = sequelize.define('WordListStat', {
    tests: DataTypes.TEXT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
  WordListStat.associate = function (models) {
    WordListStat.belongsTo(models.WordList, {
      foreignKey: 'wordListId',
    });
  };
  return WordListStat;
};
