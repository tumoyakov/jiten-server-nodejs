'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordAnt = sequelize.define(
        'WordAnt',
        {
            keb: DataTypes.STRING,
            reb: DataTypes.STRING,
            numSense: DataTypes.INTEGER,
        },
        { timestamps: false }
    );
    WordAnt.associate = function (models) {
        WordAnt.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordAnt;
};
