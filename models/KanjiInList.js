'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiInList = sequelize.define(
        'KanjiInList',
        {
            kanjiId: DataTypes.INTEGER,
            kanjiListId: DataTypes.INTEGER,
            createdAt: DataTypes.DATE,
            updatedAt: DataTypes.DATE
        },
        {timestamps: true}
    );
    return KanjiInList;
};