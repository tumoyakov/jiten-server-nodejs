'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordXref = sequelize.define(
        'WordXref',
        {
            keb: DataTypes.STRING,
            reb: DataTypes.STRING,
            numSense: DataTypes.INTEGER,
        },
        { timestamps: false }
    );
    WordXref.associate = function (models) {
        WordXref.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordXref;
};
