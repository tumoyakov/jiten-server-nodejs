'use strict';
module.exports = (sequelize, DataTypes) => {
  const Kanji = sequelize.define(
    'Kanji',
    {
      literal: DataTypes.STRING,
      codepoint: DataTypes.STRING,
      grade: DataTypes.STRING,
      strokeCount: DataTypes.INTEGER,
      freq: DataTypes.INTEGER,
      jlpt: DataTypes.INTEGER,
    },
    { timestamps: false }
  );
  Kanji.associate = function (models) {
    Kanji.belongsTo(models.Radical, {
      foreignKey: 'radicalId',
    });
    Kanji.hasMany(models.KanjiVariant, {
      foreignKey: 'kanjiId',
      as: 'kanjiVariants',
    });
    Kanji.hasMany(models.KanjiDicRef, {
      foreignKey: 'kanjiId',
      as: 'kanjiDicRefs',
    });
    Kanji.hasMany(models.KanjiQueryCode, {
      foreignKey: 'kanjiId',
      as: 'kanjiQueryCodes',
    });
    Kanji.hasMany(models.KanjiReading, {
      foreignKey: 'kanjiId',
      as: 'kanjiReadings',
    });
    Kanji.hasMany(models.KanjiMeaning, {
      foreignKey: 'kanjiId',
      as: 'kanjiMeanings',
    });
    Kanji.hasMany(models.Nanori, {
      foreignKey: 'kanjiId',
      as: 'nanories',
    });
    Kanji.belongsToMany(models.KanjiList, {
      through: 'KanjiInLists',
      foreignKey: 'kanjiId',
      otherKey: 'kanjiListId',
    });
    Kanji.belongsToMany(models.Radk, {
      through: 'RadToKanji',
      foreignKey: 'kanjiId',
      otherKey: 'radId',
    });
  };
  return Kanji;
};
