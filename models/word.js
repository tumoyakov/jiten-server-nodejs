"use strict";
module.exports = (sequelize, DataTypes) => {
  const Word = sequelize.define(
    "Word",
    {
      entSeq: DataTypes.STRING,
    },
    { timestamps: false }
  );
  Word.associate = function (models) {
    Word.hasMany(models.WordKanjiElement, {
      foreignKey: "wordId",
      as: "kanjiElements",
    });
    Word.hasMany(models.WordReadElement, {
      foreignKey: "wordId",
      as: "readElements",
    });
    Word.hasMany(models.WordSense, {
      foreignKey: "wordId",
      as: "senses",
    });
    Word.belongsToMany(models.WordList, {
      through: "WordInLists",
      foreignKey: "wordId",
      otherKey: "wordListId",
    });
  };
  return Word;
};
