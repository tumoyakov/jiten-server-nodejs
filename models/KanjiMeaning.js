'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiMeaning = sequelize.define(
        'KanjiMeaning',
        {
            meaning: DataTypes.STRING,
            lang: DataTypes.STRING,
            source: DataTypes.STRING
        },
        {timestamps: false}
    );
    KanjiMeaning.associate = function(models) {
        KanjiMeaning.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return KanjiMeaning;
};