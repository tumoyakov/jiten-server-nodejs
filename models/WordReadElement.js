'use strict';

module.exports = (sequelize, DataTypes) => {
  const WordReadElement = sequelize.define(
    'WordReadElement',
    {
      reb: DataTypes.STRING,
      reNoKanji: DataTypes.BOOLEAN,
    },
    { timestamps: false }
  );
  WordReadElement.associate = function (models) {
    WordReadElement.hasMany(models.WordReRestr, {
      foreignKey: 'rebId',
      as: 'reRestrs',
    });
    WordReadElement.belongsToMany(models.Abbreviation, {
      through: 'WordReInf',
      foreignKey: 'rebId',
      otherKey: 'abbrId',
    });
    WordReadElement.belongsToMany(models.Priority, {
      through: 'WordRePri',
      foreignKey: 'rebId',
      otherKey: 'priId',
    });
  };
  return WordReadElement;
};
