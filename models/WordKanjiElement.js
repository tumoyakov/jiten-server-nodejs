'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordKanjiElement = sequelize.define(
        'WordKanjiElement',
        {
            keb: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordKanjiElement.associate = function (models) {
        WordKanjiElement.belongsToMany(models.Abbreviation, {
            through: 'WordKeInf',
            foreignKey: 'kebId',
            otherKey: 'abbrId',
        });
        WordKanjiElement.belongsToMany(models.Priority, {
            through: 'WordKePri',
            foreignKey: 'kebId',
            otherKey: 'priId',
        });
    };
    return WordKanjiElement;
};
