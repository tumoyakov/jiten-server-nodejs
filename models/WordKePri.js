'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordKePri = sequelize.define(
        'WordKePri',
        {
            kebId: DataTypes.INTEGER,
            priId: DataTypes.INTEGER,
        },
        { timestamps: false }
    );
    return WordKePri;
};
