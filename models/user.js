"use strict";
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      login: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {}
  );
  User.associate = function (models) {
    User.hasMany(models.KanjiList, {
      onDelete: "CASCADE",
      foreignKey: "userId",
      as: "kanjiLists",
    });
    User.hasMany(models.WordList, {
      onDelete: "CASCADE",
      foreignKey: "userId",
      as: "wordLists",
    });
  };
  return User;
};
