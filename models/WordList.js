"use strict";

module.exports = (sequelize, DataTypes) => {
  const WordList = sequelize.define(
    "WordList",
    {
      name: DataTypes.STRING,
      userId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {}
  );
  WordList.associate = function (models) {
    const { User, Word } = models;
    WordList.belongsTo(User, {
      foreignKey: "userId",
    });
    WordList.belongsToMany(Word, {
      through: "WordInLists",
      foreignKey: "wordListId",
      otherKey: "wordId",
    });
  };
  return WordList;
};
