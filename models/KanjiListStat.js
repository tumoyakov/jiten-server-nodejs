'use strict';
module.exports = (sequelize, DataTypes) => {
  const KanjiListStat = sequelize.define('KanjiListStat', {
    tests: DataTypes.TEXT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
  KanjiListStat.associate = function (models) {
    KanjiListStat.belongsTo(models.KanjiList, {
      foreignKey: 'kanjiListId',
    });
  };
  return KanjiListStat;
};
