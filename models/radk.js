'use strict';
module.exports = (sequelize, DataTypes) => {
  const Radk = sequelize.define(
    'Radk',
    {
      radical: DataTypes.STRING,
      strokeCount: DataTypes.INTEGER,
    },
    { timestamps: false }
  );
  Radk.associate = function (models) {
    Radk.belongsToMany(models.Kanji, {
      through: 'RadToKanji',
      foreignKey: 'radId',
      otherKey: 'kanjiId',
    });
    Radk.belongsToMany(models.Radk, {
      through: 'RadToRad',
      foreignKey: 'radId',
      otherKey: 'linkedRadId',
      as: 'linkedRads',
    });
    Radk.belongsToMany(models.Radk, {
      through: 'RadToRad',
      foreignKey: 'linkedRadId',
      otherKey: 'radId',
      as: 'RadsThatHasLink',
    });
  };
  return Radk;
};
