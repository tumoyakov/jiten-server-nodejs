'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordSInf = sequelize.define(
        'WordSInf',
        {
            data: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordSInf.associate = function (models) {
        WordSInf.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordSInf;
};
