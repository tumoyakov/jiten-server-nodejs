'use strict';

module.exports = (sequelize, DataTypes) => {
  const RadToRad = sequelize.define(
    'RadToRad',
    {
      radId: DataTypes.INTEGER,
      linkedRadId: DataTypes.INTEGER,
    },
    { timestamps: false }
  );
  return RadToRad;
};
