'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordStagk = sequelize.define(
        'WordStagk',
        {
            data: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordStagk.associate = function (models) {
        WordStagk.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordStagk;
};
