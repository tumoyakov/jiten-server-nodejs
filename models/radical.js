'use strict';
module.exports = (sequelize, DataTypes) => {
    const Radical = sequelize.define(
        'Radical',
        {
            num: DataTypes.STRING,
            type: DataTypes.STRING,
            radical: DataTypes.STRING,
            meaning: DataTypes.STRING,
            freq: DataTypes.INTEGER,
            strokeCount: DataTypes.INTEGER
        },
        {timestamps: false}
    );
    Radical.associate = function(models) {
        Radical.hasMany(models.Kanji, {
            onDelete: 'CASCADE',
            foreignKey: 'radicalId',
            as: 'kanjis'
        });
    };
    return Radical;
};
