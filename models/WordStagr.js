'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordStagr = sequelize.define(
        'WordStagr',
        {
            data: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordStagr.associate = function (models) {
        WordStagr.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordStagr;
};
