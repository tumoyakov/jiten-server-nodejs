'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiQueryCode = sequelize.define(
        'KanjiQueryCode',
        {
            code: DataTypes.STRING,
            type: DataTypes.STRING
        },
        {timestamps: false}
    );
    KanjiQueryCode.associate = function(models) {
        KanjiQueryCode.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return KanjiQueryCode;
};