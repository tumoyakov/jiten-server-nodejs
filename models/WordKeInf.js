'use strict';
/*
This is a coded information field related specifically to the 
	orthography of the keb, and will typically indicate some unusual
	aspect, such as okurigana irregularity.
*/
module.exports = (sequelize, DataTypes) => {
    const WordKeInf = sequelize.define(
        'WordKeInf',
        {
            kebId: DataTypes.INTEGER,
            abbrId: DataTypes.INTEGER,
        },
        { timestamps: false }
    );
    return WordKeInf;
};
