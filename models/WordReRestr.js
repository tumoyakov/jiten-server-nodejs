'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordReRestr = sequelize.define(
        'WordReRestr',
        {
            data: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordReRestr.associate = function (models) {
        WordReRestr.belongsTo(models.WordReadElement, {
            foreignKey: 'rebId',
        });
    };
    return WordReRestr;
};
