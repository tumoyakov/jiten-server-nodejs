'use strict';

module.exports = (sequelize, DataTypes) => {
  const Abbreviation = sequelize.define(
    'Abbreviation',
    {
      abbr: DataTypes.STRING,
      full: DataTypes.STRING,
    },
    { timestamps: false }
  );
  Abbreviation.associate = function (models) {
    Abbreviation.belongsToMany(models.WordKanjiElement, {
      through: 'WordKeInf',
      foreignKey: 'abbrId',
      otherKey: 'kebId',
    });
    Abbreviation.belongsToMany(models.WordReadElement, {
      through: 'WordReInf',
      foreignKey: 'abbrId',
      otherKey: 'rebId',
    });
    Abbreviation.belongsToMany(models.WordSense, {
      through: 'WordPos',
      foreignKey: 'abbrId',
      otherKey: 'senseId',
    });
    Abbreviation.belongsToMany(models.WordSense, {
      through: 'WordField',
      foreignKey: 'abbrId',
      otherKey: 'senseId',
    });
    Abbreviation.belongsToMany(models.WordSense, {
      through: 'WordMisc',
      foreignKey: 'abbrId',
      otherKey: 'senseId',
    });
    Abbreviation.belongsToMany(models.WordSense, {
      through: 'WordDial',
      foreignKey: 'abbrId',
      otherKey: 'senseId',
    });
  };

  return Abbreviation;
};
