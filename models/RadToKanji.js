'use strict';

module.exports = (sequelize, DataTypes) => {
  const RadToKanji = sequelize.define(
    'RadToKanji',
    {
      radId: DataTypes.INTEGER,
      kanjiId: DataTypes.INTEGER,
    },
    { timestamps: false }
  );
  return RadToKanji;
};
