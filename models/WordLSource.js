'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordLSource = sequelize.define(
        'WordLSource',
        {
            lang: DataTypes.STRING,
            data: DataTypes.STRING,
            lsWasei: DataTypes.STRING,
            lsType: DataTypes.STRING,
        },
        { timestamps: false }
    );
    WordLSource.associate = function (models) {
        WordLSource.belongsTo(models.WordSense, {
            foreignKey: 'senseId',
        });
    };
    return WordLSource;
};
