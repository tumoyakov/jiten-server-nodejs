"use strict";
module.exports = (sequelize, DataTypes) => {
  const WordInList = sequelize.define(
    "WordInList",
    {
      wordId: DataTypes.INTEGER,
      wordListId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    { timestamps: true }
  );
  return WordInList;
};
