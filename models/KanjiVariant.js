'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiVariant = sequelize.define(
        'KanjiVariant',
        {
            code: DataTypes.STRING,
            type: DataTypes.STRING
        },
        {timestamps: false}
    );
    KanjiVariant.associate = function(models) {
        KanjiVariant.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return KanjiVariant;
};
