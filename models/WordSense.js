'use strict';

module.exports = (sequelize, DataTypes) => {
    const WordSense = sequelize.define('WordSense', {}, { timestamps: false });
    WordSense.associate = function (models) {
        WordSense.hasMany(models.WordStagk, {
            foreignKey: 'senseId',
            as: 'stagks',
        });
        WordSense.hasMany(models.WordStagr, {
            foreignKey: 'senseId',
            as: 'stagrs',
        });
        WordSense.hasMany(models.WordXref, {
            foreignKey: 'senseId',
            as: 'xrefs',
        });
        WordSense.hasMany(models.WordAnt, {
            foreignKey: 'senseId',
            as: 'ants',
        });
        WordSense.belongsToMany(models.Abbreviation, {
            through: 'WordPos',
            foreignKey: 'senseId',
            otherKey: 'abbrId',
            as: 'positions',
        });
        WordSense.belongsToMany(models.Abbreviation, {
            through: 'WordField',
            foreignKey: 'senseId',
            otherKey: 'abbrId',
            as: 'fields',
        });
        WordSense.belongsToMany(models.Abbreviation, {
            through: 'WordMisc',
            foreignKey: 'senseId',
            otherKey: 'abbrId',
            as: 'miscs',
        });
        WordSense.belongsToMany(models.Abbreviation, {
            through: 'WordDial',
            foreignKey: 'senseId',
            otherKey: 'abbrId',
            as: 'dials',
        });
        WordSense.hasMany(models.WordLSource, {
            foreignKey: 'senseId',
            as: 'lsources',
        });
        WordSense.hasMany(models.WordGloss, {
            foreignKey: 'senseId',
            as: 'glosses',
        });
        WordSense.hasMany(models.WordSInf, {
            foreignKey: 'senseId',
            as: 'sinfs',
        });
    };
    return WordSense;
};
