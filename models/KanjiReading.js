'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiReading = sequelize.define(
        'KanjiReading',
        {
            reading: DataTypes.STRING,
            type: DataTypes.STRING
        },
        {timestamps: false}
    );
    KanjiReading.associate = function(models) {
        KanjiReading.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return KanjiReading;
};