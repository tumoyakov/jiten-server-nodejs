'use strict';
module.exports = (sequelize, DataTypes) => {
  const WordStat = sequelize.define('WordStat', {
    attempts: DataTypes.TEXT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
  WordStat.associate = function (models) {
    WordStat.belongsTo(models.Word, {
      foreignKey: 'wordId',
    });
    WordStat.belongsTo(models.User, {
      foreignKey: 'userId',
    });
  };
  return WordStat;
};
