'use strict';
module.exports = (sequelize, DataTypes) => {
    const KanjiDicRef = sequelize.define(
        'KanjiDicRef',
        {
            code: DataTypes.STRING,
            type: DataTypes.STRING
        },
        {timestamps: false}
    );
    KanjiDicRef.associate = function(models) {
        KanjiDicRef.belongsTo(models.Kanji, {
            foreignKey: 'kanjiId'
        });
    };
    return KanjiDicRef;
};