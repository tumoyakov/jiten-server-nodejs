'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordRePris', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            priId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Priorities',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
            rebId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordReadElements',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordRePris');
    },
};
