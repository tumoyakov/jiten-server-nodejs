'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordKeInfs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            abbrId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Abbreviations',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
            kebId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordKanjiElements',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordKeInfs');
    },
};
