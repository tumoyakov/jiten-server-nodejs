'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordLSources', {
            //check this
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            lang: {
                type: Sequelize.STRING,
            },
            data: {
                type: Sequelize.TEXT,
            },
            lsWasei: {
                type: Sequelize.STRING,
            },
            lsType: {
                type: Sequelize.STRING,
            },
            senseId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordSenses',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordLSources');
    },
};
