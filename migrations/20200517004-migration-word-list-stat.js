'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('WordListStats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tests: Sequelize.TEXT,
      createdAt: {
        type: Sequelize.DATE,
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      wordListId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'WordLists',
          },
          key: 'id',
        },
        allowNull: false,
        onDelete: 'cascade',
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('WordListStats');
  },
};
