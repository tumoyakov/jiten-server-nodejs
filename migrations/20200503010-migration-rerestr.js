'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordReRestrs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            data: {
                type: Sequelize.STRING,
            },
            rebId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordReadElements',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordReRestrs');
    },
};
