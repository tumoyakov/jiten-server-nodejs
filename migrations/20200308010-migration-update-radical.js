'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.addColumn(
                    'Radicals',
                    'strokeCount',
                    { type: Sequelize.INTEGER },
                    {transaction: t}),
            ])
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn('Radicals', 'strokeCount', {transaction: t}),
            ]);
        })
    }
};