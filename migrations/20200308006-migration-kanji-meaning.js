'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('KanjiMeanings', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            meaning: {
                type: Sequelize.STRING
            },
            lang: {
                type: Sequelize.STRING
            },
            source: {
                type: Sequelize.STRING
            },
            kanjiId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Kanjis'
                    },
                    key: 'id'
                },
                allowNull: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('KanjiMeanings');
    }
};
