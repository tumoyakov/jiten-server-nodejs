'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordReadElements', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            reb: {
                type: Sequelize.STRING,
            },
            reNoKanji: {
                type: Sequelize.BOOLEAN,
            },
            wordId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Words',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordReadElements');
    },
};
