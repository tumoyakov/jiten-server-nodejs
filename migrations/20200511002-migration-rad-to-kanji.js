'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RadToKanjis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      radId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Radks',
          },
          key: 'id',
        },
        allowNull: false,
        onDelete: 'cascade',
      },
      kanjiId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Kanjis',
          },
          key: 'id',
        },
        allowNull: false,
        onDelete: 'cascade',
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('RadToKanjis');
  },
};
