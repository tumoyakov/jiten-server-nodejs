'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordSInfs', {
            //check this
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            data: {
                type: Sequelize.STRING,
            },
            senseId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordSenses',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordSInfs');
    },
};
