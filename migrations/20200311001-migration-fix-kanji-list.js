'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.addColumn(
                    'KanjiLists',
                    'name',
                    { type: Sequelize.STRING },
                    {transaction: t}),
                queryInterface.addColumn(
                    'KanjiLists',
                    'createdAt',
                    { type: Sequelize.DATE },
                    {transaction: t}),
                queryInterface.addColumn(
                    'KanjiLists',
                    'updatedAt',
                    { type: Sequelize.DATE },
                    {transaction: t}),
            ])
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn('KanjiLists', 'name', {transaction: t}),
                queryInterface.removeColumn('KanjiLists', 'createdAt', {transaction: t}),
                queryInterface.removeColumn('KanjiLists', 'updatedAt', {transaction: t}),
            ]);
        })
    }
};
