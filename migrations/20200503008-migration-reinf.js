'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordReInfs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            abbrId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Abbreviations',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
            rebId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordReadElements',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordReInfs');
    },
};
