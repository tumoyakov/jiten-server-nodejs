'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.addColumn(
                    'Radicals',
                    'radical',
                    { type: Sequelize.STRING },
                    {transaction: t}),
                queryInterface.addColumn(
                    'Radicals',
                    'meaning',
                    { type: Sequelize.STRING },
                    {transaction: t}),
                queryInterface.addColumn(
                    'Radicals',
                    'freq',
                    { type: Sequelize.INTEGER },
                    {transaction: t}),
            ])
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn('Radicals', 'radical', {transaction: t}),
                queryInterface.removeColumn('Radicals', 'meaning', {transaction: t}),
                queryInterface.removeColumn('Radicals', 'freq', {transaction: t}),
            ]);
        })
    }
};
