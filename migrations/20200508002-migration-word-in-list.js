"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("WordInLists", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      wordListId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "WordLists",
          },
          key: "id",
        },
        allowNull: false,
        onDelete: "cascade",
      },
      wordId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Words",
          },
          key: "id",
        },
        allowNull: false,
        onDelete: "cascade",
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("WordInLists");
  },
};
