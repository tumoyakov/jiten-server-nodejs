'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('WordPoses', {
      //check this
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      abbrId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Abbreviations',
          },
          key: 'id',
        },
        allowNull: false,
        onDelete: 'cascade',
      },
      senseId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'WordSenses',
          },
          key: 'id',
        },
        allowNull: false,
        onDelete: 'cascade',
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('WordPoses');
  },
};
