'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordKanjiElements', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            keb: {
                type: Sequelize.STRING,
            },
            wordId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Words',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordKanjiElements');
    },
};
