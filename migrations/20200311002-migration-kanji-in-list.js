'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('KanjiInLists', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            kanjiListId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'KanjiLists'
                    },
                    key: 'id'
                },
                allowNull: false
            },
            kanjiId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Kanjis'
                    },
                    key: 'id'
                },
                allowNull: false
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('KanjiInLists');
    }
};
