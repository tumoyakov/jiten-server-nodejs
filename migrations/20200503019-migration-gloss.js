'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordGlosses', {
            //check this
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            lang: {
                type: Sequelize.STRING,
            },
            data: {
                type: Sequelize.TEXT,
            },
            type: {
                type: Sequelize.STRING,
            },
            source: {
                type: Sequelize.STRING,
            },
            senseId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordSenses',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordGlosses');
    },
};
