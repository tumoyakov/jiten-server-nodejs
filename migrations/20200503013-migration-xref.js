'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordXrefs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            keb: {
                type: Sequelize.STRING,
            },
            reb: {
                type: Sequelize.STRING,
            },
            numSense: {
                type: Sequelize.INTEGER,
            },
            senseId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordSenses',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordXrefs');
    },
};
