'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Kanjis', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            literal: {
                type: Sequelize.STRING
            },
            codepoint: {
                type: Sequelize.STRING
            },
            grade: {
                type: Sequelize.STRING
            },
            strokeCount: {
                type: Sequelize.INTEGER
            },
            freq: {
                type: Sequelize.INTEGER
            },
            jlpt: {
                type: Sequelize.INTEGER
            },
            radicalId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Radicals'
                    },
                    key: 'id'
                },
                allowNull: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Kanjis');
    }
};
