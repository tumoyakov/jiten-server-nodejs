'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('WordKePris', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            priId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Priorities',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
            kebId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'WordKanjiElements',
                    },
                    key: 'id',
                },
                allowNull: false,
                onDelete: 'cascade',
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WordKePris');
    },
};
