'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('KanjiDicRefs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            code: {
                type: Sequelize.STRING
            },
            type: {
                type: Sequelize.STRING
            },
            kanjiId: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'Kanjis'
                    },
                    key: 'id'
                },
                allowNull: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('KanjiDicRefs');
    }
};
